# %% [markdown]
# # Diffusion Processes on Complex Networks - Lab

# %% [markdown]
# ## Assignment 3

# %% [markdown]
# Link to my GitLap repository with solution: https://gitlab.com/249755/diffusion-processes-on-complex-networks.git

# %% [markdown]
# ### Ex 1

# %%
# import os
# !{sys.executable} -m pip install powerlaw

# %% [markdown]
# Class Graph imported from previous assignment:

# %%
from l2 import *

# %% [markdown]
# Needed imports:

# %%
import numpy as np
import matplotlib.pyplot as plt
from scipy.special import comb
from fractions import Fraction
import random
import seaborn as sns
import powerlaw

# %% [markdown]
# Functions for getting basic measures and degrees:

# %%
def degrees(graph):
    degrees = []
    for vertex in graph.get_vertices():
        degrees.append(len(graph.get_vertex(vertex).connected_to))
    return {'degrees': degrees, 'avg': np.mean(degrees), 'std': np.std(degrees)}

# %%
def measures(graph):
    measures = {}
    measures['nr of vertices'] = graph.num_vertices
    measures['nr of edges'] = graph.num_edges
    d = degrees(graph)
    measures['avg degree'] = d['avg']
    measures['std of degrees'] = d['std']
    return measures

# %% [markdown]
# ### Implement the following models in the language of your preference:

# %% [markdown]
# #### Random graph (Erdos-Renji)

# %%
def Erdos_Renji(N, p):
    g = Graph()
    connections = []
    for i in range(N-1):
        for j in range (i+1, N):
            r = np.random.rand()
            if r<p:
                connections.append([i, j])
    g.add_edges_from_list(connections)
    return g

# %%
def plot_ER(graph, N, p):
    d = degrees(graph)['degrees']
    plt.hist(d, density=True, bins=50, alpha = 0.3)
    left, right = plt.xlim()
    x = np.arange(int(left), int(right), 5)
    c = []
    for i in x:
        c.append(float(comb(N-1, int(i), 
            exact=True)*Fraction(p)**int(i)*((1-Fraction(p))**(N-1-int(i)))))
    plt.stem(x, c, 'r', use_line_collection=True)
    plt.legend(['empirical', 'theoretical'])
    plt.title(f'Frequency plot for the degrees for p={p}')

# %% [markdown]
# ##### For $p=0.1$:

# %%
N = 2000
p = 0.1

# %%
graphER = Erdos_Renji(N, p)

# %%
plot_ER(graphER, N, p)

# %%
measures(graphER)

# %% [markdown]
# Expected nr of edges is:

# %%
p*N*(N-1)/2

# %% [markdown]
# ##### For $p=0.2$:

# %%
p = 0.2

# %%
graphER = Erdos_Renji(N, p)

# %%
plot_ER(graphER, N, p)

# %%
measures(graphER)

# %% [markdown]
# Expected nr of edges is:

# %%
p*N*(N-1)/2

# %% [markdown]
# ##### For $p=0.5$:

# %%
p = 0.5

# %%
graphER = Erdos_Renji(N, p)

# %%
plot_ER(graphER, N, p)

# %%
measures(graphER)

# %% [markdown]
# Expected nr of edges is:

# %%
p*N*(N-1)/2

# %% [markdown]
# #### Watts-Strogatz model

# %%
def Watts_Strogatz(N, K, beta):
    if K%2==0 and N>K:
        vertices = list(range(N))
        k = [int(x) for x in list(np.arange(-K/2,0,1)) + list(np.arange(1,K/2+1,1))]
        connections = []
        for i in range(N):
            for j in k:
                if [i, vertices[(i+j)%(N)]][::-1] not in connections:
                    connections.append([i, vertices[(i+j)%(N)]])
        new_connections = []
        for con in connections:
            r = np.random.rand()
            if r<beta:
                f = con[0]
                t = random.choice(vertices)
                while t==f or ([f, t] or [t, f]) in (connections or new_connections):
                    t = random.choice(vertices)
                new_connections.append([f, t])
            else:
                new_connections.append(con)
        g = Graph()
        g.add_edges_from_list(new_connections)
        return g

# %%
def plot_WS(graph, N, K, beta):
    d = degrees(graph)['degrees']
    sns.histplot(d, stat='probability')
    left, right = plt.xlim()
    x = np.arange(K/2, int(right)+1, 1)
    c = []
    for i in x:
        k = int(i)
        f = min(k - K/2, K/2)
        n = np.arange(0, f+1, 1)
        c.append(sum((comb(K/2, n)*(1-beta)**n*beta**(K/2-n)*(beta*K/2)**(k-K/2-n)*
                np.exp(-beta*K/2))/[np.math.factorial(int(k-K/2-i)) for i in n]))
    plt.stem(x, c, 'r', use_line_collection=True)
    plt.title(rf'Frequency plot for the degrees for $K={K}$ and $\beta={beta}$')

# %% [markdown]
# ##### For $\beta=0.1$:

# %%
N = 2000
K = 4
beta = 0.1

# %%
graphWS = Watts_Strogatz(N, K, beta)

# %%
plot_WS(graphWS, N, K, beta)

# %%
measures(graphWS)

# %% [markdown]
# ##### For $\beta=0.2$:

# %%
beta = 0.2

# %%
graphWS = Watts_Strogatz(N, K, beta)

# %%
plot_WS(graphWS, N, K, beta)

# %%
measures(graphWS)

# %% [markdown]
# ##### For $\beta=0.3$:

# %%
beta = 0.3

# %%
graphWS = Watts_Strogatz(N, K, beta)

# %%
plot_WS(graphWS, N, K, beta)

# %%
measures(graphWS)

# %% [markdown]
# #### Barabasi-Albert model

# %%
def Barabasi_Albert(N, k0, k):
    connections = []
    vertices_degrees = dict(zip(list(range(k0)), [k0-1 for i in range(k0)]))
    for i in range(k0):
        for j in range(i, k0):
            connections.append([i, j])
    for i in range(k0, N):
        to_vertices = np.random.choice(list(vertices_degrees.keys()), k, 
            replace = False, p=np.array(list(vertices_degrees.values()))/
            sum(list(vertices_degrees.values())))
        connections.extend([[i, t] for t in to_vertices])
        for t in to_vertices:
            vertices_degrees[t] += 1
        vertices_degrees[i] = 1
    g = Graph()
    g.add_edges_from_list(connections)
    return g

# %%
def plot_BA(graph, N, k0, k):
    d = degrees(graph)['degrees']
    fit = powerlaw.Fit(d)
    fig2 = fit.plot_ccdf(color='b')
    fit.power_law.plot_ccdf(color='b', linestyle='--', ax=fig2)
    plt.legend(['empirical', 'theoretical'])
    plt.title(f'Tail of CDF for k={k}')

# %% [markdown]
# ##### For $k=1$:

# %%
N = 2000
k0 = 5
k = 1

# %%
graphBA = Barabasi_Albert(N, k0, k)

# %%
plot_BA(graphBA, N, k0, k)

# %%
measures(graphBA)

# %% [markdown]
# ##### For $k=2$:

# %%
k = 2

# %%
graphBA = Barabasi_Albert(N, k0, k)

# %%
plot_BA(graphBA, N, k0, k)

# %%
measures(graphBA)

# %% [markdown]
# ##### For $k=5$:

# %%
k = 5

# %%
graphBA = Barabasi_Albert(N, k0, k)

# %%
plot_BA(graphBA, N, k0, k)

# %%
measures(graphBA)

# %% [markdown]
# ### Ex 2

# %%
import networkx as nx
from bs4 import BeautifulSoup
import requests
import pickle
import plotly.express as px
import pandas as pd
import plotly

# %% [markdown]
# #### Write a function that fetches a list of friends of a test user valerois and transforms it into a graph
# 

# %%
def list_of_friends(user):
    url = 'https://www.livejournal.com/misc/fdata.bml?user=' + user
    req = requests.get(url)
    ht = BeautifulSoup(req.text, "html.parser")
    text = str(ht)
    friends = []
    for line in text.split('\n'):
        if len(line.split(' ')) == 2:
            friends.append(line.split(' ')[1])
    return friends

# %%
def frieds_graph(user, level):
    list_from = [user]
    connections = []
    while level>0:
        new_list_from = []
        for usr in list_from:
            lof = list_of_friends(usr)
            connections.extend([(usr, friend) for friend in lof])
            if level > 1:
                new_list_from.extend(lof)
        list_from = new_list_from
        level -= 1
    g = nx.Graph()
    g.add_edges_from(connections)
    return g

# %%
# graph_social = frieds_graph('valerois', 2)

# %% [markdown]
# #### Save the results into a file

# %%
# with open('graph_social.pickle', 'wb') as f:
#     pickle.dump(graph_social, f)

# %%
with open('graph_social.pickle', 'rb') as f:
    graph_social = pickle.load(f)

# %% [markdown]
# #### Determine the number of nodes and edges in the network

# %% [markdown]
# Number of nodes:

# %%
graph_social.number_of_nodes()

# %% [markdown]
# Number of edges:

# %%
graph_social.number_of_edges()

# %% [markdown]
# #### Find celebrities in the network

# %% [markdown]
# Top 10 od nodes with highest degree:

# %%
sorted(graph_social.degree, key=lambda x: x[1], reverse=True)[:10]

# %% [markdown]
# #### Plot the degree distribution of the network

# %%
degrees_social = pd.DataFrame(list(dict(graph_social.degree).values()), 
                                            columns = ['degree'])
plotly.offline.init_notebook_mode()
px.histogram(degrees_social, title='Histogram of degrees')

# %% [markdown]
# Histogram is visible and interactive in html view, but I did a screenshot of it for pdf:

# %% [markdown]
# ![hist1](newplot1.png)

# %% [markdown]
# I also did a screenshot of a part of it for better viev:

# %% [markdown]
# ![hist](newplot.png)


