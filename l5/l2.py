# %% [markdown]
# # Diffusion Processes on Complex Networks - Lab
# ## Assignment 2

# %% [markdown]
# Link to my GitLap repository with solution: https://gitlab.com/249755/diffusion-processes-on-complex-networks.git

# %% [markdown]
# ### Write your own implementation of the graph structure

# %%
import sys
import queue
import matplotlib.pyplot as plt
import numpy as np

# %% [markdown]
# #### Class Vertex

# %% [markdown]
# Class for storing data of single vertex.

# %%
class Vertex:
    '''Implementation of the vertex structure.
    
    It requires id of vertex to initialize (int, string, etc.).
    Class Vertex stores id of vertex and its neighbors.
    
    Attributes:
        id: Key of vertex.
        connected_to (dict): Dictionary which contains vertices connected to vertex, 
            as keys, and weights of connections, as values.
        dist (int): Atrribute needed for searching the shortest paths. 
            Distance from vertex to another vertex.

    '''
    def __init__(self, num):
        self.id = num
        self.connected_to = {}
        self.dist = sys.maxsize

    def add_neighbor(self, nbr, weight=1):
        '''Method add_neighbor adds neigbor with weight to vertex.

        Args:
            nbr (Vertex): Object of class Vertex which is neighbor of vertex.
            weight (float): Weight of connection. Default weight is 1.

        '''
        self.connected_to[nbr] = weight

    def get_connections(self):
        '''Method get_connections return neigbors of vertex.

        Returns:
            list(self.connected_to.keys()) (list): List of vertices 
                which are neighbors of vertex.

        '''
        return list(self.connected_to.keys())   

    def set_distance(self, d):
        '''Method set_distance sets attribute self.dist.

        Args:
            d (int): Distance from vertex to another vertex.
            
        '''
        self.dist = d

    def get_distance(self):
        '''Method get_distance which returns self.dist attribute.

        Returns:
            self.dist (int): Distance from vertex to another vertex.

        '''
        return self.dist

# %% [markdown]
# #### Class Graph

# %%
class Graph:
    ''' Implementation of the undirected graph structure.
    
    It doesn't require arguments for initialize.
    Class Graph is needed for create graph, add nodes and edges.
    It makes possible finding shortest paths, saving graph, etc.

    Attributes:
        vert_list (dict): Dictionary which contains vertices with id of vertex, 
            as a key, and object Vertex, as a value.
        num_vertices (int): Number of vertices in graph.
        num_edges (int): Number of edges in graph.

    '''
    def __init__(self):
        self.vert_list = {}
        self.num_vertices = 0
        self.num_edges = 0

    def add_vertex(self, key):
        '''Method add_vertex adds a new vertex to graph and returns it.

        Args:
            key (int, string, etc.): Id of a new vertex.

        Returns:
            new_vertex (Vertex): Added vertex, object of Vertex class.
        
        '''
        if key not in self:
            self.num_vertices += 1
            new_vertex = Vertex(key)
            self.vert_list[key] = new_vertex
            return new_vertex

    def add_vertices_from_list(self, vert_list):
        '''Method add_vertices_from_list adds new vertices from list of keys of vertices.

        Args:
            vert_list (list): List of id's of new vertices which we want to add.

        '''
        for key in vert_list:
            self.add_vertex(key)
    
    def add_edge(self, from_vert, to_vert, weight = 1):
        '''Method add_edge adds edge with weight between two vertices.

        Args:
            from_vert (int, string, etc.): Id of vertex where the edge begins.
            to_vert (int, string, etc.): Id of vertex where the edge ends.
            weight (float): Weight of connection. Default weight is 1.

        '''
        if from_vert not in self.vert_list:
            self.add_vertex(from_vert)
        if to_vert not in self.vert_list:
            self.add_vertex(to_vert)
        self.vert_list[from_vert].add_neighbor(self.vert_list[to_vert], weight)
        self.vert_list[to_vert].add_neighbor(self.vert_list[from_vert], weight)
        self.num_edges += 1
    
    def add_edges_from_list(self, edge_list):
        '''Method add_edges_from_list adds new edges from list of lists 
            of from_vert and to_vert. It can contain weights too.

        Args:
            edge_list (list): List of lists of edge data.
                Example of edge_list: [[1, 2], [2, 3, 0.5], [3, 4, 0.7]].
        '''
        for edge_data in edge_list:
            if len(edge_data) == 2:
                self.add_edge(edge_data[0], edge_data[1])
            else:
                self.add_edge(edge_data[0], edge_data[1], edge_data[2])

    def get_vertices(self):
        '''Method get_vertices returns list of id's of vertices from graph.
        
        Returns:
            list(self.vert_list.keys()) (list): List of id's of vertices from graph.

        '''
        return sorted(list(self.vert_list.keys()))

    def get_vertex(self, n):
        '''Method get_vertex returns object of class Vertex for given vertex id.
        
        Args:
            n (int, string, etc.): Id of vertex which we want to get.

        Returns:
            self.vert_list[n] (Vertex): Object of class Vertex with id == n.
                None if n not in vert_list.
        
        '''
        if n in self.vert_list:
            return self.vert_list[n]
        else:
            return None

    def get_edges(self):
        '''Method get_edges returns list of edges of graph.

        Returns:
            edges (list): List of edges from graph.PNG
                Example return: [[1, 2], [2, 3], [3, 4]].
            
        '''
        edges = []
        for ver in self.get_vertices():
            for con in self.get_vertex(ver).get_connections():
                if [ver, con.id][::-1] not in edges:
                    edges.append([ver, con.id])
        return edges

    def get_neighbors(self, vert_key):
        '''Method get_neighbors returns all neighbors of vertex given by its id.
        
        Args:
            vert_key (int, string, etc.): Id of vertex whose neighbors we want to get.

        Returns:
            [con.id for con in self.get_vertex(vert_key).get_connections()] (list): 
                List of id's of neighbors of given vertex.

        '''
        return [con.id for con in self.get_vertex(vert_key).get_connections()]

    def __iter__(self):
        '''Method responsible for iteration of class Graph.

        It gives an opportunity to check if vertex with given id is in the graph
            using in operator.
        
        Returns:
            iter(self.vert_list.keys()) (iterator): 
                Iteration over id's of vertices from graph.
        
        '''
        return iter(self.vert_list.keys()) 

    def save_graph(self, name_of_file):
        '''Method save_graph writes dot representation of the graph to a text file.

        Args:
            name_of_file (string): Name of file where we want to save graph.

        '''
        text = 'graph G {\n'
        for edge in self.get_edges():
            text += '"' + str(edge[0]) + '"' + '--' + '"' + str(edge[1]) + '"' + '\n'
        text += '}'
        name = name_of_file + '.txt'
        f = open(name, 'w')
        f.write(text)
        f.close()

    def bfs(self, start):
        '''Method bfs is needed for searching something in graph.

        It starts with given vertex and sets dist attribute 
            of all another vertices from graph. 
        dist attribute is a distance from start vertex.

        Args:
            start (Vertex): Vertex from which we start bfs algorithm.
            
        '''
        visited = set()
        visited.add(start)
        start.set_distance(0)
        vert_queue = queue.Queue()
        vert_queue.put(start)
        while vert_queue.qsize() > 0:
            current_vert = vert_queue.get()
            for nbr in current_vert.get_connections():
                if nbr not in visited:
                    nbr.set_distance(current_vert.get_distance() + 1)
                    visited.add(nbr)
                    vert_queue.put(nbr)
        
    def get_shortest_paths(self, start):
        '''Method get_shortest_paths returns lengths of shortest paths 
            from given vertex to all another vertices from graph.

            It uses bfs method.

            Args:
                start (Vertex): Vertex from which we want to get the shortest paths.

            Returns:
                shortest_paths (list): List of lists, where we have 
                    id of vertex from graph and its distance from start vertex.

        '''
        shortest_paths = []
        self.bfs(start)
        for vertex in self.get_vertices():
            if self.get_vertex(vertex) != start:
                shortest_paths.append([vertex, self.get_vertex(vertex).get_distance()])
        return shortest_paths

# %% [markdown]
# #### Test

# %% [markdown]
# A simple check if my implementation works properly.

# %% [markdown]
# ##### Adding all edges to the graph and getting vertices:

# %%
# g = Graph()
# g.add_edges_from_list([['Alice', 'Bob'], ['Carl', 'Alice'], ['Alice', 'David'], 
#     ['Alice', 'Ernst'], ['Alice', 'Frank'], ['Bob', 'Gail'], ['Gail', 'Harry'], 
#     ['Harry', 'Jen'], ['Jen', 'Gail'], ['Harry', 'Irene'], ['Irene', 'Gail'], 
#     ['Irene', 'Jen'], ['Ernst', 'Frank'], ['David', 'Carl'], ['Carl', 'Frank']])
# g.get_vertices()

# # %% [markdown]
# # ##### Checking if vertex is in graph:

# # %%
# 'Alice' in g

# # %%
# 'Kasia' in g

# # %% [markdown]
# # ##### Saving graph to .txt file:

# # %%
# g.save_graph('graph')

# # %% [markdown]
# # A dot representation of the graph is available in graph.txt file.
# # 
# # After pasting text from file into http://www.webgraphviz.com/ I got the following graph:
# # 
# # ![graph](graph.PNG)

# # %% [markdown]
# # ##### Getting the edges of graph:

# # %%
# g.get_edges()

# # %% [markdown]
# # ##### Calculating shortest paths in the graph from the given vertex to all other vertices:

# # %%
# g.get_shortest_paths(g.get_vertex('Irene'))


